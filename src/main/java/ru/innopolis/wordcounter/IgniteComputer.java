package ru.innopolis.wordcounter;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.wordcounter.resources.Resource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class IgniteComputer {

    private static final Logger logger = LoggerFactory.getLogger(IgniteComputer.class);

    private IgniteConfiguration igniteConfiguration;
    private static final Map<String, Integer> EXCEPTION_MAP = new HashMap<>();

    static {
        EXCEPTION_MAP.put("Ошибка", 0);
    }

    public IgniteComputer() {
    }

    public IgniteConfiguration getIgniteConfiguration() {
        return igniteConfiguration;
    }

    public void setIgniteConfiguration(IgniteConfiguration igniteConfiguration) {
        this.igniteConfiguration = igniteConfiguration;
    }

    public Collection<Map<String, Integer>> getEachResourceWordCountMapsCollection (List<Resource> argsForComputing){

        try(Ignite ignite = Ignition.start(igniteConfiguration)){

            IgniteCompute igniteCompute = ignite.compute();

            return igniteCompute.apply((Resource arg) -> {

                try {
                    arg.initText();
                } catch (FileNotFoundException e) {
                    logger.error("Файл {} не найден.", arg.getInputParam());
                    return EXCEPTION_MAP;
                } catch (IOException e1) {
                    logger.error("{} call executed ", e1);
                    return EXCEPTION_MAP;
                }

                if (!checkResourceText(arg)){
                    return EXCEPTION_MAP;
                }

                Map<String, Integer> singleResult = new HashMap<>();

                String[] currentStringSplit = arg.getText().split(" ");
                for (String word : currentStringSplit){
                    System.out.println("Current word is " + word);
                    if (singleResult.containsKey(word)){
                        singleResult.put(word, singleResult.get(word) + 1);
                    } else {
                        singleResult.put(word, 1);
                    }
                }

                return singleResult;
            }, argsForComputing);
        }
    }

    private boolean checkResourceText(Resource resource){

        if (!resource.isTextCorrect()){

            String resourceShortName;

            if (resource.getInputParam().length() >= 20) {
                resourceShortName = resource.getInputParam().substring(0, 19) + "...";
            } else {
                resourceShortName = resource.getInputParam();
            }

            logger.error("Ресурс {} содержит недопустимые символы.", resourceShortName);
            return false;
        } else {
            return true;
        }
    }
}
