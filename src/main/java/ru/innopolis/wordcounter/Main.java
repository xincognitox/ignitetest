package ru.innopolis.wordcounter;

import org.apache.ignite.configuration.IgniteConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.innopolis.wordcounter.resources.Resource;
import ru.innopolis.wordcounter.resources.ResourceFactory;

import java.util.*;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger("ru.innopolis.wordcounter");

    public static void main(String[] args) {

        if (args.length == 0){
            logger.info("У программы не заданы аргументы. Работа завершена.");
            return;
        }

        List<Resource> resources = new ArrayList<>();
        for (String arg : args) {
            Resource resource = ResourceFactory.createResourceWithParam(arg);
            resources.add(resource);
        }

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(new String[]{"app-context.xml"});
        IgniteConfiguration igniteConfiguration = (IgniteConfiguration) applicationContext.getBean("ignite.cfg");

        IgniteComputer igniteComputer = new IgniteComputer();
        igniteComputer.setIgniteConfiguration(igniteConfiguration);
        System.out.println(sumMaps(igniteComputer.getEachResourceWordCountMapsCollection(resources)));

    }

    private static Map<String, Integer> sumMaps (Collection<Map<String, Integer>> arg){

        Map<String, Integer> result = new HashMap<>();

        for (Map<String, Integer> i : arg) {

            for (Map.Entry<String, Integer> entry : i.entrySet()){
                String word = entry.getKey();
                if (result.containsKey(word)){
                    result.put(word, result.get(word) + entry.getValue());
                } else {
                    result.put(word, entry.getValue());
                }
            }
        }
        return result;
    }
}
