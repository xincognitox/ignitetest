package ru.innopolis.wordcounter.constants;

import java.util.regex.Pattern;

/**
 * В этом классе хранятся константы.
 */
public class Constants {

    /**
     * Регулярное выражение, описывающее запрещённые в тексте символы (запрещены все, кроме перечисленных).
     */
    public static final Pattern UNAVAILABLE_SYMBOLS = Pattern.compile("[^а-яА-Я0-9\\p{Punct}\\s]");

    /**
     * Регулярное выражение пути к txt файлу или названия txt файла.
     */
    public static final String TXT_FILE_REGEX =
            "(^([a-zA-Z]\\:|\\\\)\\\\([^\\\\]+\\\\)*[^\\/:*?\"<>|]+\\.txt)|([^\\/:*?\"<>|]+\\.txt)$";

}
