package ru.innopolis.ignitetest;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCluster;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.Ignition;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class ComputeGridWithLoad {
    public static void main(String[] args) {
        int totalCount = 0;

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(new String[]{"appContext.xml"});

        List<List<Integer>> argsForComputing = separateList();

        IgniteConfiguration igniteConfiguration = (IgniteConfiguration) applicationContext.getBean("ignite.cfg");

        try(Ignite ignite = Ignition.start(igniteConfiguration);) {

            IgniteCluster cluster = ignite.cluster();

            ClusterGroup readyNodes = cluster.forPredicate((node)
                    -> node.metrics().getCurrentCpuLoad() < 0.5);

            IgniteCompute igniteCompute = ignite.compute(readyNodes);

            Collection<Integer> result = igniteCompute.apply((List<Integer> arg) -> {
                int count = 0;
                System.out.println("Started computing");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for(int i: arg){
                    System.out.println(" Current digit is " + i);
                    if((i%2) == 0) {
                        count++;
                    }
                }
                return count;
            }, argsForComputing);


            for(int i: result){
                System.out.println("result = " + i);
                totalCount+=i;
            }
            System.out.println("TotalCount = " + totalCount);
        }

      /*  IgniteConfiguration igniteConfiguration = (IgniteConfiguration) applicationContext.getBean("ignite.cfg");
        try (Ignite ignite = Ignition.start(igniteConfiguration);) {
            System.out.println("Success started");
            Cache<Integer, String> cache = ignite.getOrCreateCache("testCache");
            *//*for (int i = 0; i < 100; i++) {
                cache.put(i, "value number " + i);
            }
*//*
            for (int i = 0; i < 100; i++) {
                System.out.println(cache.get(i));
            }

        }*/
    }
    private static List<List<Integer>> separateList() {
        List<List<Integer>> result = new ArrayList<>();
        result.add(getArgList());
        result.add(getArgList());
        result.add(getArgList());
        return result;
    }

    private static List<Integer> getArgList() {
        List<Integer> result = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 50; i++) {
            int val = random.nextInt();
            result.add(val);
        }
        return  result;
    }
}