package ru.innopolis.ignitetest;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class IgniteCache {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(new String[]{"app-context.xml"});

        IgniteConfiguration igniteConfiguration = (IgniteConfiguration) applicationContext.getBean("ignite.cfg");

        try(Ignite ignite = Ignition.start(igniteConfiguration)){

            System.out.println("Started");
            javax.cache.Cache<Integer, String> cache = ignite.getOrCreateCache("testCache");

            for (int i = 0; i < 100; i++){
                System.out.println(cache.get(i));
            }
        }
    }
}
