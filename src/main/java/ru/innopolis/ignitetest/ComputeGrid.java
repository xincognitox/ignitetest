package ru.innopolis.ignitetest;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ComputeGrid {

    public static void main(String[] args) {

        List<List<Integer>> argsForComputing = separateList();

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(new String[]{"app-context.xml"});

        IgniteConfiguration igniteConfiguration = (IgniteConfiguration) applicationContext.getBean("ignite.cfg");

        try(Ignite ignite = Ignition.start(igniteConfiguration)){

            IgniteCompute igniteCompute = ignite.compute();
            Collection<Integer> result = igniteCompute.apply((List<Integer> arg) -> {
                int sum = 0;
                for (int i : arg) {
                    System.out.println("Current digit is " + i);
                    sum += i;
                }
                return sum;
            }, argsForComputing);

            for (int i : result){
                System.out.println("Result = " + i);
            }
        }
    }

    private static List<List<Integer>> separateList() {
        List<List<Integer>> result = new ArrayList<>();
        result.add(newList());
        result.add(newList());
        result.add(newList());
        return result;
    }

    private static List<Integer> newList() {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < 100; i++){
            result.add((int) (Math.random() * 100));
        }
        return result;
    }
}
